from django.db import models


class Statistic(models.Model):
    data = models.CharField(max_length=30)
    followers = models.BigIntegerField()

    def __int__(self):
        return self.followers

    def __str__(self):
        return self.data

