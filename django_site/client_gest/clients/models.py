from django.db import models


class Document(models.Model):
    num_doc = models.CharField(max_length=50)

    def __str__(self):
        return self.num_doc


class Person(models.Model):
    tags = models.CharField(max_length=30)
    comment = models.TextField()

    def __str__(self):
        return self.tags + ' ' + self.comment
