# Para executar

 - ## Django
   - Passo 1: Crie o banco:
     - python manage.py makemigrations
     
   - Passo 2: Execute as migrações:
     - python manage.py migrate

   - Passo 3: Execute o servidor:
     - python manage.py runserver

- ## Heroku
    - passo 1: logar:
        - heroku login

    - Passo 2: acesse o respositório:
         - heroku git:clone -a nomeDoPrograma
     
    - Passo 3: crie a migração no heroku:
        - heroku run manage.py migrate 
    
    - Passo 4: adicione o conteudo:
        - git add .
     
    - Passo 5: adicione o comentário de mudança:
         - git commit -am "make it better"

    - Passo 6: faça upload no git:
        - git push heroku master

## Versão beta disponível em :  http://snapfollows.herokuapp.com