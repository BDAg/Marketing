from flask import Flask, flash, Response
from flask import request, session, abort
from flask import render_template
from flask import redirect
from flask import url_for
from flask import json
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from werkzeug.wrappers import BaseRequest
from werkzeug.wsgi import responder
from werkzeug.exceptions import HTTPException, NotFound
from flask_login import login_user, logout_user
from flask_migrate import Migrate, MigrateCommand
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import DataRequired
import pymysql

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root@localhost/InstaMarketing'
app.config['SECRET_KEY'] = 'any secret string'

db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

lm = LoginManager(app)

class User(db.Model):

    __tablename__ = 'InstaMarketing'
    _id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nome = db.Column(db.String(40))
    nomeUsuario = db.Column(db.String(40))
    email = db.Column(db.String(100))
    senha = db.Column(db.String(30))
    senhaConfirmacao = db.Column(db.String(30))     
    
    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True
    
    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def __repr__(self):
        return "<User %r>" % self.email

    def __init__(self, nome, nomeUsuario, email, senha, senhaConfirmacao):
        self.nome = nome    
        self.nomeUsuario = nomeUsuario    
        self.email = email    
        self.senha = senha    
        self.senhaConfirmacao = senhaConfirmacao

db.create_all()    

@app.route('/delete', methods=["GET", "POST"])
def delete():
    dadosBanco = User('nome', 'nomeUsuario', 'email', 'senha', 'senhaConfirmacao')
    nome = request.form.get('nome')
    nomeUsuario = request.form.get("nomeUsuario")
    email = request.form.get('email')
    senha = request.form.get('senha')
    senhaConfirmacao = request.form.get('senhaConfirmacao')
    username = User.query.filter_by(nomeUsuario=nome).first()
    
    if username and username.senha and username.nomeUsuario and username.senhaConfirmacao and username.email:
        db.session.delete(username)
        db.session.commit()
        return "conta deletada"
            
    return render_template('deleteConta.html')

@app.route('/update', methods=["GET", "POST"])
def update():
    nomeAtual = request.form.get("nomeAtual")
    nomeNew = request.form.get('nome')
    nomeUsuarioNew = request.form.get("nomeUsuario")
    emailNew = request.form.get('email')
    senhaNew = request.form.get('senha')
    senhaConfirmacaoNew = request.form.get('senhaConfirmacao')
    dadosNovos =User(nomeNew, nomeUsuarioNew, emailNew, senhaNew, senhaConfirmacaoNew)
    
    username = User.query.filter_by(nome=nomeAtual).first()

    if username and username.nomeUsuario and username.senha and username.senhaConfirmacao:
        db.session.delete(username) 
        db.session.add(dadosNovos)
        db.session.commit()

    return render_template('update.html')

@app.route("/login", methods=["POST", "GET"])
def login():
    form = LoginForm()
    if request.method == "POST":
        nomeUser = request.form["nomeUser"]
        senhaUser = request.form["senhaUser"]
        username = User.query.filter_by(nomeUsuario=nomeUser).first()
        if username and username.senha == form.senhaUser.data:
            return redirect(url_for('tag'))

        else:
            redirect(url_for('cadastrar'))
    return render_template("login.html")

class LoginForm(FlaskForm):
    nomeUsuario = StringField('nomeUser', validators=[DataRequired()])
    senhaUser = PasswordField("senhaUser", validators=[DataRequired()])

@app.route('/cadastrar', methods=['GET', 'POST'])
def cadastrar():
    error = None
    if(request.method == "POST"):
        nome = (request.form.get('nome'))
        nomeUsuario = (request.form.get('nomeUsuario'))
        email = (request.form.get('email'))
        senha = (request.form.get('senha'))
        senhaConfirmacao = (request.form.get('senhaConfirmacao'))
        
        if (nome != '' and nomeUsuario != '' and email != '' and senha != '' and senhaConfirmacao != ''):
            dado = User(nome, nomeUsuario, email, senha, senhaConfirmacao)
            db.session.add(dado)
            db.session.commit()
            
            return redirect(url_for('tag'))
        else:
            return 'Insira os dados.'           
    return render_template('cadastro.html')

@app.route('/perfil', methods=["GET", "POST"])
def perfil():
# 	# U = Login(nomeUser)
#     # usuarios = User.query.filter_by(nome=U).first()
    usuarios = User.query.all()
    return render_template("perfil.html", usuarios=usuarios)

@app.route("/home", methods=["GET", "POST"])
def home():
    error = None
    # if request.method == "POST":
    
    return render_template("home.html")

# Tags ===============================================================

class tagMarketing(db.Model):
    __tablename__ = 'tagInstaMarketing'
    _id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    tag = db.Column(db.String(40))

    def __init__(self, tag):
        self.tag = tag

db.create_all()

@app.route('/tag', methods=['GET', 'POST'])
def tag():
    error = None
    if (request.method == "POST"):
        tag = request.form.get('tag')
        dado = tagMarketing(tag)
        db.session.add(dado)
        db.session.commit()
        
        if delete:
            redirect(url_for('delete'))
    return render_template('tag.html')

@app.route("/mostrarTags", methods=["GET","POST"])
def mostrarTags():
    tag = tagMarketing.query.all()
    return render_template("mostrarTags.html", tag=tag)

@app.route('/tagDelete', methods=["GET", "POST"])
def deleteTag():
    tags = tagMarketing('tag')
    tag = request.form.get('deletetag')
    tagBanco = tagMarketing.query.filter_by(tag=tag).first()

    if tagBanco:
        db.session.delete(tagBanco)
        db.session.commit()
        flash("Tag deletada!")

    if tagBanco == False:
        flash("Tag não existe no banco")

    return render_template('deleteTag.html')

@app.route('/updateTag', methods=["GET", "POST"])
def updateTag():
    tagAtual = request.form.get("tagAtual")
    tagNova = request.form.get("tagNova")
    dadoNovo = tagMarketing(tagNova)
    dado = tagMarketing.query.filter_by(tag=tagAtual).first()
    db.session.delete(dado)
    
    db.session.add(dadoNovo)
    db.session.commit()

    return render_template("updateTag.html")

if __name__ == "__main__":
    app.run(debug=True)