import pymongo

class Atlas(object):

    connection = None
    collectiondb = None
    db = None

    def __init__(self):
        print('\nInitializing Atlas')

    def connect(self, user, password, cluster, host, collectiondb):
        self.connection = pymongo.MongoClient("mongodb+srv://"+str(user)+":"+password+"@"+cluster+"-"+host+".mongodb.net/test")
        self.collectiondb = collectiondb

    def dataFind(self, content, data):
        self.content = content
        self.data = data
        self.db = self.connection.test
        return self.db[self.collectiondb].find({content: data[content]})

    def dataFindAll(self, content, parameter):
        self.content = content
        self.parameter = parameter
        db = self.connection.test
        cur = db[self.collectiondb].find({self.content: self.parameter})
        for doc in cur: print(doc)

    def dataUpdateMany(self, contentFind, parameterFind, contentUpdate, parameterUpdate,):
        self.contentFind = contentFind
        self.parameterFind = parameterFind
        self.contentUpdate = contentUpdate
        self.parameterUpdate = parameterUpdate
        db = self.connection.test
        db[self.collectiondb].update_many({contentFind: parameterFind}, {'$inc': {contentUpdate:parameterUpdate}})

    def dataUpdateOne(self, contentFind, parameterFind, contentUpdate, parameterUpdate,):
        self.contentFind = contentFind
        self.parameterFind = parameterFind
        self.contentUpdate = contentUpdate
        self.parameterUpdate = parameterUpdate
        db = self.connection.test
        db[self.collectiondb].update_one({contentFind: parameterFind}, {'$inc': {contentUpdate:parameterUpdate}})

    def dataInsert(self, data):
        self.data= data
        db = self.connection.test
        db[self.collectiondb].insert_one(data)
        return print("Iten Inserted")

    def dataDelete(self,content, parameter):
        self.content = content
        self.parameter = parameter
        db = self.connection.test
        db[self.collectiondb].remove({self.content: self.parameter})
        return print("iten deleted")
