from atlasconnect import Atlas


# def main():
    ##### instance class ######
    # atlas = Atlas()


    ##### connect your atlas ######
    # atlas.connect(
    #     user='yourUser',
    #     password='yourPassword',
    #     cluster='yourCluster',
    #     host='pmqar or j2ppt',
    #     collectiondb='your collection',
    # )


    ##### exemple data ######
    # data = {}
    # texto = 'texto de teste'
    # data.update({
    #     'source' : 'test',
    #     'content': texto,
    # })


    ################################### find iten #############################################
    # path_return = atlas.dataFind(
    #     content='content',
    #     data=data,
    # )


    ############################### Update many itens #########################################
    # atlas.dataUpdateMany(
    #     contentFind='source',
    #     parameterFind='test',
    #     contentUpdate='id',
    #     parameterUpdate=1,
    # )


    ############################### Update one Iten ###########################################
    # atlas.dataUpdateOne(
    #     contentFind='source',
    #     parameterFind='test',
    #     contentUpdate='id',
    #     parameterUpdate=2,
    # )


    ############################## find all itens #############################################
    # atlas.dataFindAll(content='source', parameter='test')


    ############################### insert using find #########################################
    # if path_return.count() == 0:
        # atlas.dataInsert(data= data)


    ############################### delete using find #########################################
    # if path_return.count() != 0:
    #     atlas.dataDelete(content='content', parameter='texto de teste')


##### function main ######
# if __name__=="__main__":
#     main()